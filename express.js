const express = require('express')
const app = express()
var port = process.env.PORT || 8000
//app.use(express.static('static'))
app.use(express.urlencoded({extended:false}))
const cors = require('cors')
app.use(cors())
app.listen(port, ()=>
    console.log(`HTTP Server with Express.js is listening on port:${port}`)
)
app.get('/',(req,res)=>{
    res.send('Microservice 1 Gateway by Hanwen Jiang and Siqiang Fan. Usage: host/Age_ChineseZodiac?year=xxxx')
})
app.get('/Age_ChineseZodiac', function(req,res){
    var year = req.query.year
    if(!Number.isInteger(Number(year)))
    {
        return res.send("Please enter a valid year number!")
    }
    const list = ["Monkey","Rooster","Dog","Pig","Rat","Ox","Tiger","Rabbit","Dragon","Snake","Horse","Goat"]
    var animal = list[year%12]
    var curYear = new Date().getFullYear()
    res.send(`You are ${curYear-year} years old and you are a ${animal}`)
})
